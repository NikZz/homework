import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main{
    public static void main(String[] args) throws IOException {
        double x;
        double y;

        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))){
            x = Double.parseDouble(bufferedReader.readLine());
            y = Double.parseDouble(bufferedReader.readLine());
        }

        if (x == 0 && y == 0) System.out.println("Center");
        else if (y == 0) System.out.println("Axis X");
        else if (x == 0) System.out.println("Axis Y");

        else if (x > 0 ) {
            if (y > 0) System.out.println("Quarter - I");
            else System.out.println("Quarter - II");
        } else if (x < 0) {
            if (y < 0) System.out.println("Quarter - III");
        } else System.out.println("Quarter - IV");
    }
}
